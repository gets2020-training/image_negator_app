all:
	gcc shm_client.c -o shm_client  -Wall -lrt -lpthread -ggdb -lm
	gcc -std=c11 -pedantic image_read.c -o image_read -lm 	
	gcc shm_server.c -o shm_server  -Werror -lrt -lpthread -ggdb 

clean:
	rm -f shm_client
	rm -f shm_server
